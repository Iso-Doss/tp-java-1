package bj.isodoss.tpjava1.controller;

import bj.isodoss.tpjava1.model.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {


    /**
     * Create - Add a customer.
     *
     * @param customer - A object of customer.
     * @return customer - A object of customer.
     */
    @PostMapping("")
    public Customer addCustomer(@RequestBody Customer customer) {
        return customer;
    }

    /**
     * Read - Get a customer.
     *
     * @param customerId - A customer id.
     * @return customer - A object of customer.
     */
    @GetMapping("/{customerId}")
    public Customer getCustomer(@PathVariable(name = "customerId") String customerId) {
        return this.makeCustomer();
    }

    /**
     * Update - Update a customer.
     *
     * @param customerId - A customer id.
     * @param customer - A object of customer.
     * @return - A object of customer.
     */
    @PutMapping("/{customerId}")
    public Customer updateCustomer(@PathVariable(name = "customerId") String customerId, @RequestBody Customer customer) {
        return this.makeCustomer();
    }

    /**
     * Delete - Delete a customer.
     *
     * @param customerId - A customer id.
     * @return - A object of customer.
     */
    @DeleteMapping("/{customerId}")
    public Customer deleteCustomer(@PathVariable(name = "customerId") String customerId) {
        return this.makeCustomer();
    }

    /**
     * Read - get all customers.
     *
     * @return - A object of customer.
     */
    @GetMapping("")
    public List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(this.makeCustomer());
        return customers;
    }

    /**
     * Make customer.
     *
     * @return - A object of customer.
     */
    private Customer makeCustomer() {
        Customer customer = new Customer();
        customer.setUserName("Iso-Doss");
        customer.setFirstName("Israel Morel");
        customer.setLastName("DOSSOU");
        //How to manage Date.
        //customer.setBirthday(null);
        return customer;
    }

}
